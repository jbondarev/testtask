# Test App
    Test app into MTS company
    Report: https://docs.google.com/document/d/1Z_lsMC3cxrPHvW88BwADOxDRKvvfygUdOTelMBPGyak/edit?usp=sharing
    
# Requierements
    Docker version 19.03.2, build 6a30dfc
    docker-compose version 1.21.2, build a133471
 
# Installation:
    $ git clone https://gitlab.com/jbondarev/testtask.git
    $ cd testTask
    $ docker-compose up -d --build
    
# How to test api
    1. Register on the site
    2. Get api key
    3. Use curl for checking api, and feed tab for tracking changes (replace {} with api-key)
        Get all values
        $ curl -v http://127.0.0.1:100/3d7e863944962f61/feed
        Get only one value
        $ curl -v http://127.0.0.1:100/3d7e863944962f61/feed/1
        POST method
        $ curl -i -H "Content-Type: application/json" -X POST -d '{"user_id": 1, "title":"some title, "content":"test content"}' http://127.0.0.1:100/{}/feed
        PUT Method
        $ curl -v '127.0.0.1:100/45c14fd8d4e11145/feed'curl -i -H "Content-Type: application/json" -X PUT -d '{"user_id": 1, "title":"another title", "content":"another content"}' http://127.0.0.1:100/45c14fd8dee11145/feed/1
        DELETE Methid
        $ curl -X DELETE http://127.0.0.1:100/45c14fd8dee11145/feed/1

        