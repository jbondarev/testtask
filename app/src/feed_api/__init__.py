from flask import Blueprint

bp = Blueprint('feed', __name__)

from src.feed_api import routes