"""
Every url here build by next rules 
<string:token>/feed/get
...
"""
import datetime

from flask import render_template, abort, jsonify, request
from flask_login import login_required
from sqlalchemy import desc

from src.feed_api import bp

# create auth decorator - if token is valid continue else -> return bad response
from src.models.posts import Post
from src.models.user import User
from src.models.user_token import UserToken
from functools import wraps


def token_auth(view):
    @wraps(view)
    def wrapper(*args, **kwargs):
        try:
            token = kwargs['token']
            if (_is_valid_token(token)):
                return view(*args, **kwargs)
            else:
                abort(401)
        except Exception as e:
            raise e

    return wrapper


@bp.route("/feed", methods=["GET"])
@login_required
def feed():
    """
    Display changes
    """
    posts = _get_good_posts_repr()
    context = {
        "posts": posts
    }
    return render_template("feed/feed.html", context=context)


def _get_good_posts_repr() -> list:
    good_posts_repr = []
    posts = Post.query.all()
    if posts:
        for post in posts:
            author = User.query.filter_by(id=post.user_id).first()
            good_posts_repr.append({
                "author": author.username,
                "title": post.title,
                "content": post.content,
            })
    return good_posts_repr


@bp.route("/<string:token>/feed", methods=["GET"])
@token_auth
def feed_get(token):
    """
    RESTful feed's function - get all feed posts
    """
    user_id_by_token = UserToken.query.filter_by(token=token).first().user_id
    posts = Post.query.filter_by(user_id=user_id_by_token).all()
    if posts:
        return jsonify(posts=[post.serialize() for post in posts]), 200
    else:
        return '', 204


@bp.route("/<string:token>/feed/<int:id>", methods=["GET"])
@token_auth
def feed_get_id(token, id):
    """
    RESTful feed's function - get feed post by id
    """
    user_id_by_token = UserToken.query.filter_by(token=token).first().user_id
    post = Post.query.filter_by(user_id=user_id_by_token, id=id).first()
    if post:
        return jsonify(post=post.serialize()), 200
    else:
        return '', 204


@bp.route("/<string:token>/feed", methods=["POST"])
@token_auth
def feed_post(token):
    """
    RESTful feed's function - get all feed posts
    """
    try:
        if not request.json:
            abort(400)
        if "user_id" in request.json and type(request.json["user_id"]) is not int:
            abort(400)
        if "title" in request.json and type(request.json["title"]) is not str:
            abort(400)
        if "description" in request.json and type(request.json["description"]) is not str:
            abort(400)

        user_id = request.json.get("user_id")
        title = request.json.get("title")
        content = request.json.get("content")
        post = Post(user_id=user_id, title=title, content=content)
        if not post.save():
            raise Exception("Not successful post transaction")
        return jsonify(post=post.serialize()), 200
    except Exception as e:
        # TODO add logger
        abort(500)


@bp.route("/<string:token>/feed/<int:id>", methods=["PUT"])
@token_auth
def feed_put(token, id):
    """
    RESTful feed's function - update feed post by id
    """
    try:
        if not request.json:
            abort(400)
        if "user_id" in request.json and type(request.json["user_id"]) is not int:
            abort(400)
        if "title" in request.json and type(request.json["title"]) is not str:
            abort(400)
        if "description" in request.json and type(request.json["description"]) is not str:
            abort(400)

        new_user_id = request.json.get("user_id")
        new_title = request.json.get("title")
        new_content = request.json.get("content")

        post = Post.query.filter_by(id=id).first()
        if post:
            post.user_id = new_user_id
            post.title = new_title
            post.content = new_content
            post.update()
            return jsonify(post=post.serialize()), 200
        else:
            return '', 204

    except Exception as e:
        abort(500)


@bp.route("/<string:token>/feed/<int:id>", methods=["DELETE"])
@token_auth
def feed_delete(token, id):
    """
    RESTful feed's function - delete feed post by id
    """
    try:
        post = Post.query.filter_by(id=id).first()
        if post:
            post.delete()
            return '', 200
        else:
            return '', 204
    except Exception as e:
        raise e


def _is_valid_token(token: str):
    try:
        user_token = UserToken.query. \
            filter_by(token=token). \
            order_by(desc(UserToken.expires_at)).first()
        if user_token and user_token.expires_at > datetime.datetime.utcnow():
            return True
        else:
            return False
    except Exception as e:
        raise e
