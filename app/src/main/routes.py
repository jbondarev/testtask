import datetime

from flask import render_template
from flask_login import login_required, current_user
from sqlalchemy import desc
from src import db
from src.main import bp
from src.models.user_token import UserToken
from src.utils.os_utils import generate_unique_token


@bp.route('/')
def index():
    return render_template('main/index.html')


@bp.route('/profile')
@login_required
def profile():
    token = _get_or_gen_user_token()
    context: dict = {
        'name': current_user.username,
        'token': token
    }
    return render_template('main/profile.html', context=context)


def _get_or_gen_user_token() -> str:
    try:
        user_token = UserToken.query. \
            filter_by(user_id=current_user.id). \
            order_by(desc(UserToken.expires_at)).first()
        expires_time = datetime.timedelta(seconds=60 * 60 * 24)
        if user_token and user_token.expires_at > datetime.datetime.utcnow():
            return user_token.token
        else:
            token = UserToken(user_id=current_user.id,
                              expires_at=datetime.datetime.utcnow() + expires_time,
                              token=generate_unique_token())
            if not token.save():
                raise Exception("Not successful token transaction")
            return token.token
    except Exception as e:
        raise e
