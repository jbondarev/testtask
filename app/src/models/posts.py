from flask import jsonify

from app import db
from src.models.mixins import DbMixin


class Post(DbMixin, db.Model):
    """
    Simple data
    """

    __tablename__ = "posts"

    # primary keys
    id = db.Column(db.Integer, primary_key=True)

    # model description

    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    title = db.Column(db.String(255))
    content = db.Column(db.Text)

    def __repr__(self) -> str:
        return f"Post<id={self.id}, token={self.title}>"

    def __str__(self) -> str:
        return self.username

    def serialize(self) -> dict:
        return {
            "id": self.id,
            "user_id": self.user_id,
            "title": self.title,
            "content": self.content
        }
