import datetime

from flask import jsonify

from app import db
from src.models.mixins import DbMixin


class UserToken(DbMixin, db.Model):
    """
    Splitting Token and User model, because probably in future
    we want to give custom rules for each token (admin token, dev token, etc...)
    """

    __tablename__ = "users_tokens"

    # primary keys
    id = db.Column(db.Integer, primary_key=True)

    # model description

    user_id = db.Column(db.Integer, db.ForeignKey("users.id"))
    token = db.Column(db.String(16))
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    expires_at = db.Column(db.DateTime)

    def __repr__(self) -> str:
        return f"UserToken<id={self.id}, token={self.token}, created_at={self.created_at}, expires_at={self.expires_at}>"

    def __str__(self) -> str:
        return self.username

    def serialize(self) -> dict:
        return {
            "id": self.id,
            "token": self.token,
            "created_at": self.created_at,
            "expires_at": self.expires_at
        }
