import binascii
import os


def get_env_variable(name: str) -> str:
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)


def generate_unique_token():
    return str(binascii.hexlify(os.urandom(8)).decode())